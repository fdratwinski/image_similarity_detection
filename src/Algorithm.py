import random

import timeit
from itertools import product

import math
import numpy as np

import Constants


def get_integrated_key_pairs(key_pairs, neighbours_for_key_pairs, integrity_threshold, number_of_neighbours):
    integrated_key_pairs = []
    key_pairs_array = np.asarray(key_pairs)
    print("Started calculating integrated key pairs")
    start_time = timeit.default_timer()
    for i in range(len(key_pairs)):
        counter = 0
        for j in range(number_of_neighbours):
            index_of_neighbour_first_point = np.where(
                np.all(neighbours_for_key_pairs[i][0][j] == key_pairs_array[:, 0], axis=1))
            if index_of_neighbour_first_point[0].shape[0] != 0 and np.where(
                    np.all(key_pairs[index_of_neighbour_first_point[0][0]][1] == neighbours_for_key_pairs[i][1],
                           axis=1))[0].shape[0] != 0:
                counter += 1
        if counter / number_of_neighbours >= integrity_threshold:
            integrated_key_pairs.append(key_pairs[i])

    print("Finished calculating integrated key pairs in time: " + str(timeit.default_timer() - start_time))
    return integrated_key_pairs


def calculate_model(key_pairs):
    transform = None
    first_image_points_small_matrix = np.ones(shape=(len(key_pairs), 3))
    first_image_points_small_matrix[:, :2] = key_pairs[:, 0, :2]
    second_image_points_vector = np.concatenate((key_pairs[:, 1, 0], key_pairs[:, 1, 1]))

    if len(key_pairs) == 3:
        first_image_points_matrix = np.concatenate((np.concatenate(
            (first_image_points_small_matrix, np.zeros(shape=(3, 3)))), np.concatenate(
            (np.zeros(shape=(3, 3)), first_image_points_small_matrix))), axis=1)
        try:
            transform = np.linalg.inv(first_image_points_matrix) @ second_image_points_vector
        except np.linalg.LinAlgError:
            return None
        transform = np.concatenate((transform, [0, 0, 1]))
    if len(key_pairs) == 4:
        combined_matrix = np.zeros(shape=(8, 2))
        combined_matrix[:4, 0] = (-1) * key_pairs[:, 1, 0] * key_pairs[:, 0, 0]
        combined_matrix[4:8, 0] = (-1) * key_pairs[:, 1, 1] * key_pairs[:, 0, 0]
        combined_matrix[:4, 1] = (-1) * key_pairs[:, 1, 0] * key_pairs[:, 0, 1]
        combined_matrix[4:8, 1] = (-1) * key_pairs[:, 1, 1] * key_pairs[:, 0, 1]

        first_image_points_matrix = np.concatenate((np.concatenate(
            (first_image_points_small_matrix, np.zeros(shape=(4, 3)))), np.concatenate(
            (np.zeros(shape=(4, 3)), first_image_points_small_matrix)), combined_matrix), axis=1)

        try:
            transform = np.linalg.inv(first_image_points_matrix) @ second_image_points_vector
        except np.linalg.LinAlgError:
            return None
        transform = np.concatenate((transform, [1]))
    return np.resize(transform, new_shape=(3, 3))


def get_random_samples(key_pairs, which_transformation, if_heuristic):
    if which_transformation == Constants.AFFINE_TRANSFORMATION:
        number_of_random_samples = 3
    else:
        number_of_random_samples = 4

    key_pairs_array = np.asarray(key_pairs)
    random_indices = -np.ones(shape=number_of_random_samples, dtype=int)
    if if_heuristic:
        r = Constants.r
        R = Constants.R
        random_indices[0] = random.randint(0, len(key_pairs) - 1)
        for i in range(1, number_of_random_samples):
            while random_indices[i] == -1:
                potential_pair = random.randint(0, len(key_pairs) - 1)
                distances = np.sum((key_pairs_array[random_indices[0]][:, :2] - key_pairs_array[potential_pair][:, :2]) ** 2,
                                   axis=1)
                if all(distances > (r ** 2)) and all(distances < (R ** 2)):
                    random_indices[i] = potential_pair
    else:
        random_indices = random.sample(range(0, len(key_pairs) - 1), number_of_random_samples)

    return np.asarray(key_pairs)[random_indices]


def model_error(model, pair):
    first_point = pair[0]
    second_point = pair[1]
    first_point_vector = np.asarray([first_point[0], first_point[1], 1])
    second_point_vector = np.asarray([second_point[0], second_point[1], 1])
    transformed_first_point = model @ first_point_vector
    transformed_first_point = transformed_first_point / transformed_first_point[2]
    return np.sum((transformed_first_point - second_point_vector) ** 2), (transformed_first_point, second_point)


def ransac(number_of_iterations, key_pairs, max_error, which_transformation, p_probability, w_probability,
           is_heuristic_estimate, is_heuristic_distances, is_heuristic_break, is_heuristic_distrubution_modification):
    best_model = None
    best_score = -np.Inf
    best_model_pairs = []
    key_pairs_cloned = key_pairs[:]
    if is_heuristic_estimate and w_probability != 0:
        if which_transformation == Constants.AFFINE_TRANSFORMATION:
            n = 3
        else:
            n = 4
        number_of_iterations = int((math.log2(1 - p_probability)) / (math.log2(1 - (w_probability ** n))))

    print(
        "Started calculating finiding best " + which_transformation + " model with RANSAC learning. Number of iterations: " + str(
            number_of_iterations))
    start_time = timeit.default_timer()
    for i in range(number_of_iterations):
        model = None
        while model is None:
            sample = get_random_samples(key_pairs_cloned, which_transformation, is_heuristic_distances)
            model = calculate_model(sample)
        score = 0
        model_pairs = []
        for pair in key_pairs:
            error, transformed_pair = model_error(model, pair)
            if error < max_error:
                model_pairs.append(transformed_pair)
                score += 1
        if score > best_score:
            best_score = score
            best_model = model
            best_model_pairs = model_pairs
            if is_heuristic_break and best_score / len(key_pairs) >= Constants.MODEL_ERROR_RATIO:
                break
            if is_heuristic_distrubution_modification:
                key_pairs_cloned.extend(sample)

    print("Finished calculating integrated key pairs in time: " + str(timeit.default_timer() - start_time))
    return best_model, best_model_pairs
