import pickle as pkl
import subprocess
import timeit

import numpy as np

import Constants
import Visualizer
import Algorithm

from PIL import Image
from gevent.libev.corecext import os


def get_params_from_image(image_name):
    opened_image = Image.open("images\\" + image_name)
    converted_image = opened_image.convert('RGB').convert('P', palette=Image.ADAPTIVE)
    converted_image.save("converted_images\\" + image_name)
    subprocess.run(
        ["extract_features\\extract_features_32bit", "-haraff", "-sift", "-i", "converted_images\\" + image_name,
         "-DE"])
    os.system("move converted_images\\" + image_name + ".h* extracted_keys\\")
    params_file = open("extracted_keys\\" + image_name + ".haraff.sift", "r")

    number_of_features = int(params_file.readline())
    number_of_key_points = int(params_file.readline())
    data_matrix = np.zeros(shape=(number_of_key_points, number_of_features + 2))
    i = 0
    for line in params_file:
        array_of_values = line.split(" ")
        data_matrix[i][0] = float(array_of_values[0])
        data_matrix[i][1] = float(array_of_values[1])
        for j in range(number_of_features):
            data_matrix[i][j + 2] = int(array_of_values[j + 5])
        i += 1
    return data_matrix


def calculate_neighbours(key_point_index, key_points, number_of_neighbours):
    key_points_without_key_point = np.concatenate(
        (key_points[:key_point_index, :], key_points[key_point_index + 1:, :]))
    neighbours = np.argpartition(np.sqrt(np.sum((key_points_without_key_point[:, :2] - np.tile(
        key_points[key_point_index][:2, ], (key_points_without_key_point.shape[0], 1))) ** 2, axis=1)),
                                 number_of_neighbours)
    return key_points_without_key_point[neighbours[:number_of_neighbours]]


def get_key_pairs_from_two_images(first_image_name, second_image_name):
    pairs_file_name = 'key_pairs//' + first_image_name + second_image_name + '.pkl'
    if os.path.isfile(pairs_file_name):
        with open(pairs_file_name, 'rb') as f:
            return pkl.load(f)
    first_image_key_points = get_params_from_image(first_image_name)
    second_image_key_points = get_params_from_image(second_image_name)
    pairs = []
    for i in range(first_image_key_points.shape[0]):
        first_image_key_point_features = first_image_key_points[i][2:]

        index_of_closest_key_point_second_image = np.argmin(np.sum(abs(
            second_image_key_points[:, 2:] - np.tile(first_image_key_point_features,
                                                     (second_image_key_points.shape[0], 1))), axis=1))

        second_image_key_point = second_image_key_points[index_of_closest_key_point_second_image][2:]

        index_of_closest_key_point_first_image = np.argmin(np.sum(
            abs(first_image_key_points[:, 2:] - np.tile(second_image_key_point, (first_image_key_points.shape[0], 1))),
            axis=1))

        if index_of_closest_key_point_first_image == i:
            pairs.append((first_image_key_points[index_of_closest_key_point_first_image],
                          second_image_key_points[index_of_closest_key_point_second_image]))
        with open(pairs_file_name, 'wb') as file:
            pkl.dump(pairs, file)
    return pairs


def get_neighbours_from_key_pairs(key_pairs, number_of_neighbours):
    neighbours = []
    first_image_key_points = np.asarray([left[0] for left in key_pairs])
    second_image_key_points = np.asarray([right[1] for right in key_pairs])
    for i in range(len(key_pairs)):
        first_key_from_pair_neighbours = calculate_neighbours(i, first_image_key_points, number_of_neighbours)
        second_key_from_pair_neighbours = calculate_neighbours(i, second_image_key_points, number_of_neighbours)
        neighbours.append((first_key_from_pair_neighbours, second_key_from_pair_neighbours))
    return neighbours


if __name__ == '__main__':
    key_pairs = get_key_pairs_from_two_images(Constants.FIRST_IMAGE_NAME,
                                              Constants.SECOND_IMAGE_NAME)
    print("Number of all pairs: " + str(len(key_pairs)))
    Visualizer.visualize_key_points_pairs(Constants.FIRST_IMAGE_NAME,
                                          Constants.SECOND_IMAGE_NAME,
                                          key_pairs)
    neighbours_for_key_pairs = get_neighbours_from_key_pairs(key_pairs,
                                                             Constants.NUMBER_OF_NEIGHBOURS)
    integrated_key_pairs = Algorithm.get_integrated_key_pairs(key_pairs,
                                                              neighbours_for_key_pairs,
                                                              Constants.INTEGRITY_THRESHOLD,
                                                              Constants.NUMBER_OF_NEIGHBOURS)
    print("Number of integrated pairs: " + str(len(integrated_key_pairs)))
    Visualizer.visualize_integrated_key_points_pairs(Constants.FIRST_IMAGE_NAME,
                                                     Constants.SECOND_IMAGE_NAME,
                                                     integrated_key_pairs)
    affine_model, affine_model_pairs = Algorithm.ransac(Constants.ITERATIONS,
                                                        key_pairs,
                                                        Constants.MAX_ERROR,
                                                        Constants.AFFINE_TRANSFORMATION,
                                                        0.5,
                                                        0 / len(key_pairs),
                                                        Constants.NO_HEURISTIC,
                                                        Constants.NO_HEURISTIC,
                                                        Constants.NO_HEURISTIC,
                                                        Constants.NO_HEURISTIC)
    print("Number of pairs calculated from affine model: " + str(len(affine_model_pairs)))
    Visualizer.visualize_affine_transformation(Constants.FIRST_IMAGE_NAME,
                                               Constants.SECOND_IMAGE_NAME,
                                               affine_model_pairs,
                                               affine_model)
    projective_model, projective_model_pairs = Algorithm.ransac(Constants.ITERATIONS,
                                                                key_pairs,
                                                                Constants.MAX_ERROR,
                                                                Constants.PROJECTIVE_TRANSFORMATION,
                                                                0.5,
                                                                0 / len(key_pairs),
                                                                Constants.NO_HEURISTIC,
                                                                Constants.NO_HEURISTIC,
                                                                Constants.NO_HEURISTIC,
                                                                Constants.NO_HEURISTIC)
    print("Number of pairs calculated from projective model: " + str(len(projective_model_pairs)))
    Visualizer.visualize_projective_transformation(Constants.FIRST_IMAGE_NAME,
                                                   Constants.SECOND_IMAGE_NAME,
                                                   projective_model_pairs,
                                                   projective_model)
