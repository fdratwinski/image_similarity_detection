import random as rand

import cv2
import numpy as np


def visualize_key_points_pairs(first_image_name, second_image_name, pairs):
    combined_image_name, first_image_height = combine_images(first_image_name, second_image_name)
    image_with_drawn_lines = draw_lines_between_pairs(cv2.imread(combined_image_name), pairs, first_image_height)
    cv2.imwrite("visualized_key_points_images\\visualized" + first_image_name + second_image_name,
                image_with_drawn_lines)
    show_image("Combined images with all key point pairs", image_with_drawn_lines.shape[1],
               image_with_drawn_lines.shape[0], image_with_drawn_lines)


def combine_images(first_image_name, second_image_name):
    first_image = cv2.imread("images\\" + first_image_name)
    second_image = cv2.imread("images\\" + second_image_name)
    stacked_image = np.vstack((first_image, second_image))
    image_name = "combined_images\\" + first_image_name + second_image_name
    cv2.imwrite(image_name, stacked_image)
    return image_name, first_image.shape[0]


def draw_lines_between_pairs(image, pairs, vertical_offset):
    for pair in pairs:
        first_image_coordinate = (int(pair[0][0]), int(pair[0][1]))
        second_image_coordinate = (int(pair[1][0]), int(pair[1][1] + vertical_offset))
        line_color = (rand.randint(0, 255), rand.randint(0, 255), rand.randint(0, 255))
        cv2.line(image, first_image_coordinate,
                 second_image_coordinate, line_color, 1, cv2.LINE_AA)
    return image


def visualize_integrated_key_points_pairs(first_image, second_image, integrated_key_pairs):
    combined_image_name, first_image_height = combine_images(first_image, second_image)
    image_with_drawn_lines = draw_lines_between_pairs(cv2.imread(combined_image_name), integrated_key_pairs,
                                                      first_image_height)
    cv2.imwrite("visualized_integrated_key_points_images\\visualized" + first_image + second_image,
                image_with_drawn_lines)
    show_image("Combined images with integrated key point pairs", image_with_drawn_lines.shape[1],
               image_with_drawn_lines.shape[0], image_with_drawn_lines)


def visualize_affine_transformation(first_image_name, second_image_name, key_pairs, affine_transformation):
    first_image = cv2.imread("images\\" + first_image_name)
    second_image = cv2.imread("images\\" + second_image_name)

    first_image = cv2.warpAffine(first_image, affine_transformation[:2], (first_image.shape[1], first_image.shape[0]))
    stacked_image = np.vstack((first_image, second_image))

    image_with_drawn_lines = draw_lines_between_pairs(stacked_image, key_pairs, first_image.shape[0])
    show_image("Affine Transformation", image_with_drawn_lines.shape[1], image_with_drawn_lines.shape[0],
               image_with_drawn_lines)


def visualize_projective_transformation(first_image_name, second_image_name, key_pairs, projective_transformation):
    first_image = cv2.imread("images\\" + first_image_name)
    second_image = cv2.imread("images\\" + second_image_name)

    first_image = cv2.warpPerspective(first_image, projective_transformation,
                                      (first_image.shape[1], first_image.shape[0]))
    stacked_image = np.vstack((first_image, second_image))

    image_with_drawn_lines = draw_lines_between_pairs(stacked_image, key_pairs, first_image.shape[0])
    show_image("Projective Transformation", image_with_drawn_lines.shape[1], image_with_drawn_lines.shape[0],
               image_with_drawn_lines)


def show_image(label, width, height, image):
    cv2.namedWindow(label, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(label, width, height)
    cv2.imshow(label, image)
    cv2.waitKey(0)
